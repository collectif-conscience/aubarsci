<img src="https://i.imgur.com/XQnjblS.png"  width="200">

Réunion et discussion pour les membres du Collectif Conscience

Mardi 12 mars 2019, 19H00

La Paillasse, Paris

[collectifconscience.org](https://collectifconscience.org)

---

### Sommaire
1. Nouvelles globales du Collectif Conscience
1. Nouvelles des projets
1. Animations festival (priorité)
1. Nouvelles sur la communication scientifique 
1. Événements à venir
1. Expression libre

---

###  Nouvelles globales du Collectif Conscience


---

### Nouvelles des projets 

Pour les projets qui veulent s'exprimer.

---

#### Science Shakers 

* "La Science sur Grand Ecran" mardi 5 mars 2019 au Dernier Bar avant la Fin du Monde, Paris
* Prochain évenment: mardi 2 avril 2019, projet EchoSciences IdF

---

### Animations festivals

Festival Play Azur à Nice 9 et 10 février 2019 - [Wiki Festival](https://gitlab.com/collectif-conscience/festivals/wikis/Play-Azur-Festival-9-et-10-f%C3%A9vrier-2019)

* "Jeu de piste à énigmes scientifiques" par Thibault

* "Fait ou fake" par Erwan

* "Big data, comment ça marche?" par Nicolas PA

* Objectif Konsçiensz 

---

#### "Jeu de piste à énigmes scientifiques" par Thibault

**Description:** Un mini jeu de piste à énigmes scientifiques pour les publics qui souhaiteraient rester quelques minutes sur le stand.

**Debrief:**
* très bien reçu, attractif
* a attiré plein de gens, tellement que a demandé de faire des créneaux de réservation
* demande une bonne préparation/transfère pour être reproduite par d'autre

---

#### "Fait ou fake" par Erwan

**Description:** 
Une activité "fact or fake". Au format "Flash cards" les visiteurs parient sur des infos scientifiques que l'on trouve sur internet.

**Debrief:**
* bonne accroche
* facile à reproduire par quelqu'un d'autre

---

#### "Big data, comment ça marche?" par Nicolas PA

**Description:** Stocker et traiter les tweets de @EnDirectDuLabo pour en sortir des informations et des visualisation intéressantes comme support pour expliquer les grands principes et outils du big data.

[Projet, présentation et code](https://gitlab.com/collectif-conscience/eddl-data)

**Debrief:**

* demande à attirer les gens, pas attractif de base
* gens intéressés, ont appris des choses
* doit être rendu plus sexy: plus visuel, manipulations physiques si possibles


---

#### Objectif Konsçiensz

**Description:** Conférence théâtrale/scénarisée avec participation du public et invités de marque, liée au projet Mondes Imaginaires

**Debrief:**
* On a pu inviter Acermandax et Astronogeek
* Super chauffage de superflame qui a amené plein de monde
* Version beta, mais excellente réception
* Vidéo de l'événement montée par Théo

---

#### Festival atures discussions

* Proposer un Science shakers dans les festivals où on va avec les acteurs locaux? 
* proposer systématiquement une conf MI et une conf Ssh?
* Faire un kakemono pour le stand

---

###  Nouvelles sur la communication scientifique 

Faire un petit topo des événements, des nouveautés, des nouveaux acteurs?

---

### Événements à venir 

* Incubateur CST du CNRS, vendredi 15 mars 2019, cf Sophie et Eléonore qui participent
* Science Shakers, mardi 2 avril 2019, projet EchoSciences IdF
* Ciné Club UNIVERS CONVERGENTS, "Total Recall", mardi 26 mars, Cinéma Grand Action, Paris.



---

### Expression libre 

N'importe quel membre souhaitant s'exprimer, présenter quelque chose.

---

Transformer en slides: https://gitpitch.com/collectif-conscience/aubarsci/master?grs=gitlab